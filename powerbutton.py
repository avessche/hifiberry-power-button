from gpiozero import Button
from subprocess import check_call
from time import sleep

# Definitions 
powerbutton = Button(17)

# Functions
def shutdown():
    check_call(['sudo', 'poweroff'])

powerbutton.when_pressed = shutdown

while True:
    sleep(60)
