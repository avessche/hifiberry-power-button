# HiFiBerry power button project

## Introduction
This project enables the user to wire a physical powerbutton with status led to a HiFiBerry AMP2+ via the GPIO pins.
The powerbutton will be able to power up and safely shutdown the RaspberryPi within the limitations of the GPIO pin usage of a HiFiBerry AMP2+ HAT.
The powerled will be on when the RaspberryPi is active and off when safely shutdown. At all time 18V power remains on the AMP2+ HAT

## Electrical wiring
![electrical-wiring](docs/img/electrical-wiring.png)

### Components
- Raspberry Pi
- HifiBerry AMP2+
- default-open push button
- resistor
- led (in button)
- diode
- wires
- soldering iron 

## Installation
1. Enable uart for the status led. Append ```enable_uart=1``` to /boot/config.txt
2. Install GPIO library ```sudo apt-get install python3-gpiozero```
3. Copy the **powerbutton.py** script to */usr/local/bin/powerbutton.py* and make it executable.
4. Copy the **safeshutdown.sh** script  to */etc/init.d/safeshutdown.sh* and make it executable.
5. Run ```sudo update-rc.d safeshutdown.sh defaults```
6. (Optional) Start the script without a reboot ```/etc/init.d/safeshutdown.sh start```

## Tested on
- HiFiBerry AMP2+
- RaspberryPi 3+
- RaspberryPi 4
